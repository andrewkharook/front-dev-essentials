var gulp        = require('gulp');
var browserSync = require('browser-sync');
var reload      = browserSync.reload;
var sass        = require('gulp-sass');
var concat      = require("gulp-concat");
var csso        = require('gulp-csso');

// browser-sync task for starting the server.
gulp.task('browser-sync', function() {
    browserSync({
        files: ["./*.html","./css/**/*.css","./js/**/*.js"],
        server: {
            baseDir: "./"
        }
    });
});

// Sass task, will run when any SCSS files change & BrowserSync
// will auto-update browsers
gulp.task('sass', function () {
    return gulp.src('./scss/**/*.scss')
        .pipe(sass({errLogToConsole: true}))
        .pipe(gulp.dest('css'))
        .pipe(reload({stream:true}));
});

// Concatenate & Minify CSS
gulp.task('css', function() {
    return gulp.src('./css/*.css')
        .pipe(concat('app.min.css'))
        .pipe(csso())
        .pipe(gulp.dest('./css/min'));
});

// Default task to be run with `gulp`
gulp.task('default', ['sass', 'css', 'browser-sync'], function () {
    gulp.watch("./scss/**/*.scss", ['sass']);
    gulp.watch("./css/**/*.css", ['css']);
});
