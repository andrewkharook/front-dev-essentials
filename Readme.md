#Front-end development essentials

##Description
A set of tools for front-end web development.

##Package contents:
- [gulp](http://gulpjs.com) (streaming build system)
- [browser-sync](http://www.browsersync.io) (synchronised browser testing with live reload)
- [gulp-sass](https://npmjs.org/package/gulp-sass) (gulp plugin for sass)
- [gulp-concat](https://www.npmjs.org/package/gulp-concat) (concatenates files)
- [gulp-csso](https://www.npmjs.org/package/gulp-csso) (optimizes and minifies css files)

##Installation:
Install gulp globally if you don't have it already

    $ sudo npm install gulp -g

Install required packages

    $ npm install
